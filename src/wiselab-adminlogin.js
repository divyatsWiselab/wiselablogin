import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-styles/color.js';
import '@webcomponents/webcomponentsjs/webcomponents-loader.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/iron-validatable-behavior/iron-validatable-behavior.js';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/paper-dialog-behavior/paper-dialog-behavior.js';
// import '@polymer/neon-animation/neon-animatable.js';
// import '@polymer/neon-animation/animations/fade-out-animation.js';
// import '@polymer/neon-animation/animations/scale-up-animation.js';


class WiselabAdminlogin extends PolymerElement {
  static get template() {
    return html`
          <custom-style>
      <style is="custom-style">
            body {
            }
            paper-card {
          	max-width: 300px;
            height:450px;
            text-align: center;

            @apply(--layout-vertical);
            /* @apply(--layout-center); */
            margin: auto;
            margin-top: 8%;
            border-radius: 15px;
            }
            .cafe-header { @apply --paper-font-headline; --paper-background-color: blue}

            paper-input {
              max-width: 200px;
              margin: auto;
              font-size: 10px;
              font-family:monospace;
            }
            .linkstyle{
              font-family:monospace;
              font-size:12px;
              color: #49a6e0;
              font-weight: 400;
              cursor: pointer;
            }
            .txtAlignCenter{
              text-align:center;
            }
            .txtAlignleft{
                text-align:left;
            }
            .login-button{
              color:#fff;
              background-color: #405f71 ;
              cursor:pointer;
              font-family:  sans-serif;
              font-weight: 400;
              border-radius: 8px;
              line-height: 35px;
              border: 0;
              word-wrap: break-word;
              width:60%;
            }

            .footer{
              color: slategrey;
              font-family:monospace;
              height: 20%;
              font-size: 11px;
            }
            .loginHeader{
              background-color: #405f71;
              border-top-left-radius:10px;
              border-top-right-radius:10px;
              height:18%
            }
            .paddingTopImg{
              padding-top:8%
            }
            .passwordlink{
              padding-left:22%
            }
            .spanFooter{
              padding-top:15px;
            }
            .overlay {
              position: absolute;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
               background-image: url('../bgImage.jpg');
              /* background-color: grey; */
            }
            .alert-button{
              color:#fff;
              background-color: #405f71 ;
              cursor:pointer;
              font-family:  sans-serif;
              font-weight: 400;
              border-radius: 8px;
              line-height: 35px;
              border: 0;
              word-wrap: break-word;
              width:70px;
            }
      </style>
      </custom-style>
      <body style="width:100%">
  <div class="overlay">
  <paper-card >
      <div class="cafe-header loginHeader" > <img src="../wiselabWhite.png" class="paddingTopImg" alt="WiseLab Login"> </div>
      <div class="card-content" style="height:300px">
      <div class="">
        <br>
        <br>
        <paper-input class="txtAlignleft" label="Benutzername" type="email" id="txtUsername">
        </paper-input>
        <paper-input class="txtAlignleft" label="Passwort" minlength="8" maxlength="25" type="password" id="txtPassword">
          <iron-icon icon="password" ></iron-icon>
        </paper-input>
          <div style="" class="passwordlink"><a class="linkstyle">Passwort vergessen?</a> </div>
            <br>
            <input type="button" value="EINLOGGEN" class="login-button" on-click= "wsAdminLogin"/>
            <br>
            <br>
            <br>
            <br>
          <div class="">
          <a class="linkstyle">Noch kein kunde?</a>
          </div>
        </div>
        </div>
        <div class=" footer" style="">
        <br>
         <span>Copyright © Mex IT GmbH 2018. </span>
        </div>
        <paper-dialog id="modalError"   modal style= "width: 400px; height:130px; background-color: #fff; border: 1px solid #efefef; border-radius:5px;">
         <div style="width:100%;padding-top:10px">
            <div style="float:left;"><img src="miscAlert.png" /> &nbsp; &nbsp; </div>
            <label  style="float:left;" id='modalErrorText' value$="[[errorMessage]]"> </label>
          </div>
            <div style="width:100%; float:right;" class="buttons">
              <paper-button dialog-confirm autofocus class="alert-button">Ok</paper-button>
            </div>
        </paper-dialog>
  </paper-card>
  </div>
<body>


    `;
  }

  static get properties() {
      return {
        errorMessage: {
          type: String,
          notify: true
        }
      }
  }

  wsAdminLogin(){
      if(this.validateLogin() == false) { this.$.modalError.open(); return false;}
       var username = this.$.txtUsername.value;
       var password = this.$.txtPassword.value;
       var url = 'http://wiselabapi.wiselab.io:9505/wiselab/auth/login';       
        var data = {"username":username,   "password" : password};
        var res;
       fetch(url, {
         method: 'POST',
          body: JSON.stringify(data),
         headers:{
           'Content-Type': 'application/json'
         }
       })
       .then(
         function(response){
           if(response.status == 200) {
              this.parentElement.selected = "landingpage";
           }
           else {
              this.$.modalErrorText.innerHTML =  response.status + ": " + response.statusText;
              this.$.modalError.open(); return false;
           }
       }.bind(this))
         .catch(error =>  {console.log('Error:', error);this.$.modalErrorText.innerHTML = error.message; this.$.modalError.open(); return false;});

  }

  validateLogin(){
     var errorMsg = '';
    var username = this.$.txtUsername.value;
    var password = this.$.txtPassword.value;
    var regExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

      if(username== ""){
        errorMsg = 'Benutzername wird benötigt!';
      }
      else if( password == ""){
        errorMsg = 'Passwort wird benötigt!';
      }
      else if(regExp.test(username) == false){
        errorMsg = 'falsche E-Mail-ID!';
      }
      else if(password.length < 8 || password.length >25){
        errorMsg = 'Die Passwortlänge sollte zwischen 8 und 25 liegen.';
      }

      if(errorMsg != '')
      {
         this.$.modalErrorText.innerHTML = errorMsg;

        return false;
      }
      else
        return true;
  }

  loginSuccessful(){
    this.Page.set('route.path', '/wiselab-appdrawer');
  }
}
window.customElements.define('wiselab-adminlogin', WiselabAdminlogin);
