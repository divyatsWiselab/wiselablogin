import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-resizable-behavior/iron-resizable-behavior.js';


import  '/src/wiselab-adminlogin.js';
import  '/src/wiselab-landingpage.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
      <style>

      </style>
      <app-location route="{{route}}" >
      </app-location>
      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <iron-pages id="mainPage" selected="[[page]]" attr-for-selected="name" role="main">

        <wiselab-adminlogin name="login"></wiselab-adminlogin>
        <wiselab-landingpage name="landingpage"></wiselab-landingpage>
      </iron-pages>
    `;
  }

      static get properties() {
        return {
          page: {
            type: String,
            reflectToAttribute: true,
            observer: '_pageChanged'
          },
          routeData: Object,
          subroute: Object
        };
      }

      static get observers() {
        return [
          '_routePageChanged(routeData.page)'
        ];
      }

      _routePageChanged(page) {
        if (!page) {
          this.page = 'login';
        } else if (['login', 'landingpage'].indexOf(page) !== -1) {
          this.page = page;
        } else {
          this.page = 'login';
        }
      }

      _pageChanged(page) {
        switch (page) {
          case 'login':
            import('./wiselab-adminlogin.js');
            break;
          case 'landingpage':
            import('./wiselab-landingpage.js');
            break;
        }
      }
}

window.customElements.define('my-app', MyApp);
