import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@webcomponents/webcomponentsjs/webcomponents-loader.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-item/paper-icon-item.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/font-roboto/roboto.js';
import '@polymer/iron-resizable-behavior/iron-resizable-behavior.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';

import '../src/wiselab-appdownload.js';
import '../src/wiselab-abmelden.js';
import '../src/wiselab-autorentool.js';
import '../src/wiselab-benutzer.js';
import '../src/wiselab-dashboard.js';
import '../src/wiselab-impressum.js';
import '../src/wiselab-module.js';
import '../src/wiselab-support.js';
import '../src/wiselab-zuweisen.js';



class WiselabLandingPage extends PolymerElement {
  static get template() {
    return html`

    <custom-style>
      <style is="custom-style">
             body {
               margin: 0;
               background-color: #eee;
             }
             .textStyle{
               font-family: 'Roboto', 'Noto', sans-serif;
               background-image:  url('../bgImage.jpg');
               height:100vh;
             }
             .blueHeader {
               background-color: #74a7d7;
               color: #fff;
               height:50px;
             }
             .blueHeader paper-icon-button {
               --paper-icon-button-ink-color: white;
             }
             .whiteHeader {
               font-weight: bold;
               background-color: white;

             }
             .iconItem {
               color: #bbbcbf;
             }
             .searchCls{
               width:150px;
               background-color: #bcbec2;
               border-radius: 2px;border-bottom-color: #bcbec2;
               border-right-color: #bcbec2;
               height:25px;
             }
             .menuCls{
               background-color:#272E34 ;
               color:#bbbcbf;
               cursor: pointer;
               height:93vh;
             }
             .toolBar{
               background-color:#5fb2f4;
               height:50px;
             }
            .spnWScls{
              color:white;
              font-size:14pt;
              padding-left:40px;
            }
             app-drawer-layout:not([narrow]) [drawer-toggle] {
               display: none;
             }
             @media only screen and (max-width:500px) {
               /* For mobile phones: */
                .menuCls {
                 height:92vh;
               }
             }
             :host {
               --app-drawer-width: 180px;

             }
        </style>
    </custom-style>


      <app-drawer-layout class="textStyle">

        <app-drawer swipe-open id="wsAppdrawer" slot="drawer">
          <app-header-layout has-scrolling-region>
           <app-header class="whiteHeader" waterfall fixed slot="header">
             <app-toolbar class="toolBar">
               <span class="spnWScls" id="spnWS"> <b>Wise</b> Lab</span>
             </app-toolbar>
           </app-header>

        <div class="menuCls">

           <paper-icon-item class="appdownload" on-click="changePage">
             <iron-icon icon="file-download" slot="item-icon"></iron-icon>
             <span>AppDownload</span>
           </paper-icon-item>
           <paper-icon-item  class="dashboard" on-click="changePage" >
             <iron-icon icon="dashboard" slot="item-icon"></iron-icon>
             <span >Dashboard</span>
           </paper-icon-item>
           <paper-icon-item class="benutzer" on-click="changePage">
             <iron-icon icon="perm-identity" slot="item-icon"></iron-icon>
             <span>Benutzer</span>
           </paper-icon-item>
           <paper-icon-item class="autorentool" on-click="changePage">
             <iron-icon icon="help" slot="item-icon"></iron-icon>
             <span>Autorentool</span>
           </paper-icon-item>
           <paper-icon-item class="iconItem">
             <iron-icon icon="forward" slot="item-icon"></iron-icon>
             <span class="zuweisen" on-click="changePage">Zuweisen</span>
           </paper-icon-item>
           <paper-icon-item class="module" on-click="changePage">
             <iron-icon icon="add-circle-outline" slot="item-icon"></iron-icon>
             <span>Module</span>
           </paper-icon-item>
           <paper-icon-item class="support" on-click="changePage">
             <iron-icon icon="add-box" slot="item-icon"></iron-icon>
             <span>Support</span>
           </paper-icon-item>
           <paper-icon-item class="impressum" on-click="changePage">
             <iron-icon icon="info" slot="item-icon"></iron-icon>
             <span>Impressum</span>
           </paper-icon-item>
           <paper-icon-item class="abmelden" on-click="changePage">
             <iron-icon icon="exit-to-app" slot="item-icon"></iron-icon>
             <span>Abmelden</span>
           </paper-icon-item>
          </div>
          </app-header-layout>
          </app-drawer>

          <app-header-layout>
          <app-header class="blueHeader" condenses reveals effects="waterfall" slot="header">
           <app-toolbar>
             <paper-icon-button icon="menu" drawer-toggle></paper-icon-button>
             <div main-title></div>
             <paper-icon-button icon="help"></paper-icon-button>
           </app-toolbar>
          </app-header>

           <div style= "margin:20px;">
             <iron-pages selected="{{pageName}}" id="wsIronPage" attr-for-selected="page-name">
               <wiselab-appdownload page-name="appdownload"></wiselab-appdownload>
               <wiselab-dashboard page-name="dashboard"></wiselab-dashboard>
               <wiselab-benutzer page-name="benutzer"></wiselab-benutzer>
               <wiselab-autorentool page-name="autorentool"></wiselab-autorentool>
               <wiselab-zuweisen page-name="zuweisen"></wiselab-zuweisen>
               <wiselab-module page-name="module"></wiselab-module>
               <wiselab-support page-name="support"></wiselab-support>
               <wiselab-impressum page-name="impressum"></wiselab-impressum>
               <wiselab-abmelden page-name="abmelden"></wiselab-abmelden>
               <wiselab-appdownload page-name="appdownload"></wiselab-appdownload>
               <wiselab-appdownload page-name="appdownload"></wiselab-appdownload>
               <wiselab-appdownload page-name="appdownload"></wiselab-appdownload>
             </iron-pages>
           </div>
          </app-header-layout>
      </app-drawer-layout>


    `;
  }


  static get properties() {
     return {
       pageName: {
         type: String,
         reflectToAttribute: true,
         value:"appdownload",

       },
     }
   }

  changePage(e){
    // alert('hi');
    this.pageName = e.currentTarget.className;
    if(this.checkIsMobile())
        this.$.wsAppdrawer.toggle();
  }
  checkIsMobile(){
    var hasTouchscreen = 'ontouchstart' in window;
    var result = hasTouchscreen;
    return result;
  }

}
window.customElements.define('wiselab-landingpage', WiselabLandingPage);
